#include <iostream>
#include "Bitmap.h"
#include <time.h>
using namespace std;

int main() {
    Bitmap* bitmap = new Bitmap();
    int size = 10000;
    time_t start;
    time_t end;
    time(&start);
    cout << start << endl;
    bitmap->init(size, size);

    Pixel* p = new Pixel();
    p->redf(0.8f);
    p->greenf(0.1f);
    p->bluef(0.2f);
    p->m_x = 0;
    p->m_y = 0;

    for (int i = 0; i < bitmap->m_width; i++) {
        p->m_red--;
        p->m_green++;
        p->m_blue++;
        p->m_x = i;
        p->m_y = i;
        bitmap->setPixel(p);
    }

    for (int x=0; x < bitmap->m_width; x++) {
        for (int y=0; y < bitmap->m_height; y++) {
            p->m_x = x;
            p->m_y = y;
            bitmap->setPixel(p);
        }
    }

    time(&end);
    cout << end << endl;

    cout << "Time elapsed: " << end-start << "s" << endl;

    return 0;
}
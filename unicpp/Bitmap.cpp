//
// Created by Michał on 08.03.2017.
//

#include "Bitmap.h"

void Bitmap::init(int width, int height) {
    this->m_height = height;
    this->m_width = width;
    int arr_len = width*height*3;
    this->m_pixels = new unsigned char[arr_len];
    for (int i = 0; i < arr_len; ++i) {
        m_pixels[i] = 0;
    }
}

int pxAddr(int width, int x, int y) {
    return (width*y*3) + (x*3);
}

Pixel *Bitmap::getPixel(int x, int y) {
    Pixel* p = new Pixel();
    int addr = pxAddr(this->m_width, x, y);
    p->m_red = this->m_pixels[addr];
    p->m_green = this->m_pixels[addr+1];
    p->m_blue = this->m_pixels[addr+2];
    p->m_x = x;
    p->m_y = y;
    return p;
}

void Bitmap::setPixel(Pixel *px) {
    int addr = pxAddr(this->m_width, px->m_x, px->m_y);
    this->m_pixels[addr] = px->m_red;
    this->m_pixels[addr+1] = px->m_green;
    this->m_pixels[addr+2] = px->m_blue;
}

Bitmap::~Bitmap() {
    delete this->m_pixels;
}

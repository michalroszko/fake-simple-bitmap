//
// Created by Michał on 08.03.2017.
//

#include "Pixel.h"

float Pixel::red() {
    return this->m_red / 255.0f;
}

float Pixel::green() {
    return this->m_green / 255.0f;
}

float Pixel::blue() {
    return this->m_blue / 255.0f;
}

int clamp(int val, int min, int max) {
    if (val > max) {
        return max;
    } else if (val < min) {
        return min;
    } else {
        return val;
    }
}

unsigned char fval_to_unsigned_char(float val) {
    return clamp((int)(val*255), 0, 255);
}

void Pixel::redf(float in) {
    this->m_red = fval_to_unsigned_char(in);
}

void Pixel::greenf(float in) {
    this->m_green = fval_to_unsigned_char(in);
}

void Pixel::bluef(float in) {
    this->m_blue = fval_to_unsigned_char(in);
}

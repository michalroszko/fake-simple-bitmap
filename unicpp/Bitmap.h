//
// Created by Michał on 08.03.2017.
//

#ifndef CPPDEMO_BITMAP_H
#define CPPDEMO_BITMAP_H

#include "Pixel.h"

class Bitmap {
public:
    virtual ~Bitmap();
    void init(int width, int height);
    unsigned char* m_pixels;
    Pixel* getPixel(int x, int y);
    void setPixel(Pixel* px);
    int m_width;
    int m_height;
};


#endif //CPPDEMO_BITMAP_H

//
// Created by Michał on 08.03.2017.
//

#ifndef CPPDEMO_PIXEL_H
#define CPPDEMO_PIXEL_H


class Pixel {
public:
    unsigned char m_red;
    unsigned char m_green;
    unsigned char m_blue;
    int m_x;
    int m_y;
    float red();
    float green();
    float blue();
    void redf(float in);
    void greenf(float in);
    void bluef(float in);

};


#endif //CPPDEMO_PIXEL_H

//
// Created by Michał on 08.03.2017.
//

#ifndef CPPDEMO_PIXEL_H
#define CPPDEMO_PIXEL_H

#include <algorithm>

namespace bmp {
    class Pixel {
    public:
        void red(int r);
        void red(float r);
        int red();
        float fred();
        void green(int g);
        void green(float g);
        int green();
        float fgreen();
        void blue(int b);
        void blue(float b);
        int blue();
        float fblue();

    private:
        int r = 0;
        int g = 0;
        int b = 0;
    };
}


#endif //CPPDEMO_PIXEL_H

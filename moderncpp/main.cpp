#include <iostream>
#include <chrono>
#include "Bitmap.h"

int main() {
    bmp::Bitmap bitmap;
    int size = 10000;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    std::cout << start.time_since_epoch().count() << std::endl;

    bitmap.init(size, size);

    bmp::Pixel p;
    p.red(0.8f);
    p.green(0.1f);
    p.blue(0.2f);

    for (int i = 0; i < size; i++) {
        p.red(p.red() - 1);
        p.green(p.green() + 1);
        p.blue(p.blue() + 1);
        bitmap.setPixel(i, i, p);
    }

    for (int x=0; x < size; x++) {
        for (int y=0; y < size; y++) {
            bitmap.setPixel(x, y, p);
        }
    }

    end = std::chrono::system_clock::now();
    std::cout << end.time_since_epoch().count() << std::endl;
    std::chrono::duration<double> duration = end - start;
    std::cout << "Elapsed time: " << duration.count() << "s" << std::endl;


    return 0;
}
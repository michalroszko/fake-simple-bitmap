//
// Created by Michał on 08.03.2017.
//

#include "Bitmap.h"

namespace bmp {
    void Bitmap::init(int width, int height) {
        this->w = width;
        this->h = height;
        pixels = new Pixel[w*h];
        for (int x = 0; x < w; ++x) {
            for (int y = 0; y < h; ++y) {
                Pixel p;
                this->pixels[pixelKey(x,y)] = p;
            }
        }
    }

    long Bitmap::pixelKey(int x, int y) {
        return y*h + x;
    }

    int Bitmap::width() {
        return this->w;
    }

    void Bitmap::setPixel(int x, int y, Pixel p) {
        this->pixels[pixelKey(x,y)] = p;
    }

    int Bitmap::height() {
        return this->h;
    }

    Pixel Bitmap::getPixel(int x, int y) {
        return this->pixels[pixelKey(x,y)];
    }

    Bitmap::~Bitmap() {
        delete pixels;
    }
}
//
// Created by Michał on 08.03.2017.
//

#ifndef CPPDEMO_BITMAP_H
#define CPPDEMO_BITMAP_H

#include <unordered_map>
#include "Pixel.h"

namespace bmp {
    class Bitmap {
    public:
        void init(int width, int height);
        Pixel getPixel(int x, int y);
        void setPixel(int x, int y, Pixel p);
        int width();
        int height();

        virtual ~Bitmap();

    private:
        long pixelKey(int x, int y);
        int w;
        int h;
        Pixel* pixels;
        //std::unordered_map<long, Pixel> pixels;
    };
}


#endif //CPPDEMO_BITMAP_H

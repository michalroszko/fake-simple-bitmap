//
// Created by Michał on 08.03.2017.
//

#include "Pixel.h"

namespace bmp {

    void Pixel::red(int r) {
        this->r = r;
    }

    void Pixel::red(float r) {
        this->r = r * 255;
    }

    void Pixel::green(int g) {
        this->g = g;

    }

    void Pixel::green(float g) {
        this->g = g * 255;
    }

    void Pixel::blue(int b) {
        this->b = b;
    }

    void Pixel::blue(float b) {
        this->b = b * 255;

    }

    int Pixel::red() {
        return this->r;
    }

    int Pixel::green() {
        return this->g;
    }

    int Pixel::blue() {
        return this->b;
    }

    float Pixel::fred() {
        return static_cast<float>(r) / 255.0f;
    }

    float Pixel::fgreen() {
        return static_cast<float>(g) / 255.0f;
    }

    float Pixel::fblue() {
        return static_cast<float>(b) / 255.0f;
    }
}

